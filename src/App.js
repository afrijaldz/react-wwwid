import React, { Component } from 'react';
import './App.css';

const Card = (props) => {
  return (
    <div style={{ margin: 10, border: '1px solid black', padding: 10 }}>
      {props.title}
    </div>
  )
};

const CardList = props => {
  return (
    <div>
      {props.items.map((item, index) => <Card {...item} key={index} />)}
    </div>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      feed: [],
      isLoaded: false,
      isCached: false,
      isFetched: false,
    };
  }

  fetchCache() {
    const url = 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2Fwwwid';
    const cacheKey = url;
    const cached = sessionStorage.getItem(cacheKey);

    if (cached !== null) {
      this.setState({
        feed: JSON.parse(cached),
        isLoaded: true,
        isCached: true,
        isFetched: false,
      });

      return;
    }

    return fetch(url).then(res => {
      if (res.status === 200) {
        res.clone().text().then(content => {
          sessionStorage.setItem(cacheKey, content);
        });
      }

      return res.json();
    }).then(data => {
      this.setState({ feed: data, isLoaded: true });
      console.log(this.state.feed);
    });
  }

  componentDidMount() {
    this.fetchCache();
  }

  render() {
    if (!this.state.isLoaded) {
      return (
        <div>
          urung bro
        </div>
      );
    }

    return (
      <div>
        <CardList items={this.state.feed.items} />
      </div>
    );
  }
}

export default App;
