import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const Header = () => {
  return (
    <div className="header">
      <h1>WWWID Challenge</h1>
    </div>
  );
}

ReactDOM.render(
  <div>
    <Header />
    <App />
  </div>
  , document.getElementById('r00t'));
registerServiceWorker();
